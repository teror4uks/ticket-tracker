import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from werkzeug.contrib.cache import MemcachedCache

def setup_cache(app):
    """
    Setup ``app.cache``.
    """
    # TODO: Support other cache type.
    servers = app.config.get('MEMCACHED_SERVERS', '').split()
    if not servers:
        servers = ['localhost:11211']

    prefix = app.config.get('MEMCACHED_PREFIX', '')
    app.cache = MemcachedCache(servers=servers, key_prefix=prefix)

def setup_prod_db(app):
    """
    Setup ``app.db``.
    """
    db_settings = app.config.get("PROD_DATABASE_INFO") 
    db_connect = "dbname='%s' user='%s' host='%s' password='%s'" % \
                    (db_settings['DB_NAME'], db_settings['DB_USER'], 
                    db_settings['DB_HOST'], db_settings['DB_PASSWD'])

    app.db = psycopg2.connect(db_connect)


def setup_test_db(app):
    """
    Setup ``app.db for tests``.
    """
    create_db(app)
    db_settings = app.config.get("TEST_DATABASE_INFO") 
    db_connect = "dbname='%s' user='%s' host='%s' password='%s'" % \
                    (db_settings['DB_NAME'], db_settings['DB_USER'], 
                    db_settings['DB_HOST'], db_settings['DB_PASSWD'])

    app.db = psycopg2.connect(db_connect)

def create_db(app):
    db_settings = app.config.get("TEST_DATABASE_INFO")
    postgres_connect = "dbname='%s' user='%s' host='%s' password='%s'" % \
                    ('postgres', 'postgres', 
                    db_settings['DB_HOST'], db_settings['DB_PASSWD'])
    
    conn = psycopg2.connect(postgres_connect)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with app.open_resource('sql/create_test_db.sql', mode='r') as f:
        with conn.cursor() as curs:
            curs.execute(f.read())
    conn.close()

def drop_db(app):
    db_settings = app.config.get("TEST_DATABASE_INFO")
    postgres_connect = "dbname='%s' user='%s' host='%s' password='%s'" % \
                    (db_settings['DB_NAME'], db_settings['DB_USER'], 
                    db_settings['DB_HOST'], db_settings['DB_PASSWD'])
    conn = psycopg2.connect(postgres_connect)
    
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with conn:
        with app.open_resource('sql/drop_test_db.sql', mode='r') as f:
            with conn.cursor() as curs:
                curs.execute(f.read())