import os
import json
import flask
import tempfile
import unittest
import psycopg2
from app import app
from utils import setup_test_db, drop_db

def init_db():
    """Initializes the database."""
    with app.open_resource('sql/schema_test.sql', mode='r') as f:
        with app.db:
            with app.db.cursor() as curs:
                curs.execute(f.read())

class TestIntegrations(unittest.TestCase):
    def setUp(self):
        # self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.testing = True
        self.app = app.test_client() 

    def test_index(self):
        response = self.app.get('/')
        assert response.status_code == 200

    def test_put_exist_ticket(self):
        data = {'status': 'ANSWERED'}
        response = self.app.put('/v1/ticket/1', data=data)
        resp = json.loads(response.data)
        new_status = resp['result'].get('status')
        assert response.status_code == 200
        assert new_status == 'ANSWERED'

    def test_post_new_ticket(self):
        data = {'email': "test100", 'text': "ABRA KADABRA", 'title': 'NEW TITLE'}
        response = self.app.post('/v1/ticket', data=data)
        assert response.status_code == 201
        resp = json.loads(response.data)
        status = resp['result'].get('status')
        assert status == 'OPEN'
    
    def test_get_not_exist_ticket(self):
        response = self.app.get('/v1/ticket/2222222')
        assert response.status_code == 400
    
    def test_add_comment_for_ticket(self):
        data = {'email':'test1@test.ru', 'text':'spam!!!', 'ticket_id':1}
        response = self.app.post('/v1/ticket/1/add-comment', data=data)
        assert response.status_code == 201

    def test_get_exist_ticket(self):
        response = self.app.get('/v1/ticket/2')
        assert response.status_code == 200

    def test_try_add_comment_to_closed_ticket(self):
        data = {'status': 'CLOSED'}
        response = self.app.put('/v1/ticket/2', data=data)
        assert response.status_code == 200
        data = {'email':'test1@test.ru', 'text':'spam!!!', 'ticket_id':2} 
        response = self.app.post('/v1/ticket/2/add-comment', data=data)
        assert response.status_code == 400

if __name__ == "__main__":
    setup_test_db(app)
    init_db()
    unittest.main(exit=False)
    app.db.close()
    drop_db(app)