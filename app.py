import os
from flask import Flask
from flask_restful import Api
from resources import Index, Ticket, TicketList, Comment
from utils import setup_cache, setup_prod_db

CUR_VER_API = 'v1'

DB_PASSWD_PROD = os.getenv('F_DB_PSWD')
DB_USER_PROD = os.getenv('F_DB_USER')
DB_NAME_PROD = os.getenv('F_DB_NAME', 'ttracker')
DB_HOST_PROD = os.getenv('F_DB_HOST', 'localhost')

DB_PASSWD_TEST = os.getenv('F_DB_PSWD')
DB_USER_TEST = os.getenv('F_DB_USER')
DB_NAME_TEST = os.getenv('F_DB_NAME', 'ttracker_test')
DB_HOST_TEST = os.getenv('F_DB_HOST', 'localhost')

app = Flask(__name__)
app.config.update(MEMCACHED_SERVERS='localhost:11211',
                  MEMCACHED_PREFIX ='tickets-')

PROD_DATABASE_INFO={
    "DB_NAME":DB_NAME_PROD,
    "DB_PASSWD":DB_PASSWD_PROD,
    "DB_USER":DB_USER_PROD,
    "DB_HOST":DB_HOST_PROD
}

TEST_DATABASE_INFO={
    "DB_NAME":DB_NAME_TEST,
    "DB_PASSWD":DB_PASSWD_TEST,
    "DB_USER":DB_USER_TEST,
    "DB_HOST":DB_HOST_TEST
}

app.config.update(
    PROD_DATABASE_INFO=PROD_DATABASE_INFO,
    TEST_DATABASE_INFO=TEST_DATABASE_INFO
)

setup_cache(app)
setup_prod_db(app)
api = Api(app)

api.add_resource(Index, '/')
api.add_resource(TicketList, '/{}/ticket'.format(CUR_VER_API))
api.add_resource(Ticket, '/{}/ticket/<int:ticket_id>'.format(CUR_VER_API))
api.add_resource(Comment, '/{}/ticket/<int:ticket_id>/add-comment'.format(CUR_VER_API))

if __name__ == "__main__":
    app.run(host='0.0.0.0')