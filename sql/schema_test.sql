CREATE TYPE status_type AS ENUM('OPEN', 'ANSWERED', 'CLOSED', 'AWAIT');

CREATE TABLE "comment" (
    "id" serial NOT NULL PRIMARY KEY,
    "date_creation" timestamp with time zone NOT NULL, 
    "email" varchar(254) NOT NULL, 
    "text" text NOT NULL
);

CREATE TABLE "ticket" (
    "id" serial NOT NULL PRIMARY KEY, 
    "create_date" timestamp with time zone NOT NULL, 
    "update_date" timestamp with time zone NOT NULL, 
    "title" varchar(255) NOT NULL, 
    "text" text NOT NULL, 
    "email" varchar(254) NOT NULL, 
    "status" status_type NOT NULL DEFAULT enum_first(null::status_type)
);

ALTER TABLE "comment" ADD COLUMN "ticket_id" integer NOT NULL;
CREATE INDEX "comment_ticket_id" ON "comment" ("ticket_id");
ALTER TABLE "comment" ADD CONSTRAINT "comment_ticket_id_fk_ticket_id" FOREIGN KEY ("ticket_id") REFERENCES "ticket" ("id") DEFERRABLE INITIALLY DEFERRED;
-- ALTER TABLE "ticket" ALTER COLUMN "status" SET default 0;

-- CHECK ROWS ON VALID FOR TICKETS TABLE
CREATE or REPLACE FUNCTION valid_ticket_status() RETURNS trigger AS $valid_ticket_status$
    BEGIN
        -- Check status when update row
        IF OLD.status = 'OPEN'::status_type AND NOT (NEW.status = 'OPEN'::status_type OR NEW.status = 'ANSWERED'::status_type OR NEW.status = 'CLOSED'::status_type) THEN
            RAISE NOTICE 'NEW.status: % ', (NEW.status = 'OPEN'::status_type OR NEW.status = 'ANSWERED'::status_type OR NEW.status = 'CLOSED'::status_type);
            RAISE EXCEPTION 'New status for opened tickets should be only OPEN or ANSWERED or CLOSED';
        ELSIF OLD.status = 'ANSWERED'::status_type AND NOT (NEW.status = 'ANSWERED'::status_type OR NEW.status = 'CLOSED'::status_type OR NEW.status = 'AWAIT'::status_type) THEN
            RAISE EXCEPTION 'New status for answered tickets should be only AWAIT or CLOSED';
        ELSIF OLD.status = 'AWAIT'::status_type AND NOT (NEW.status = 'AWAIT'::status_type OR NEW.status = 'CLOSED'::status_type) THEN
            RAISE EXCEPTION 'New status for answered tickets should be only or CLOSED';
        ELSIF OLD.status = 'CLOSED'::status_type THEN
            RAISE EXCEPTION 'Closed tickets can not be change';
        -- ELSE     
        END IF;
        NEW.update_date := now();
        RETURN NEW;
    END;
$valid_ticket_status$ LANGUAGE plpgsql;
--- UPDATE ticket SET status = 'CLOSED'::status_type WHERE id = 3;
--- select * from ticket;
CREATE TRIGGER valid_ticket_status BEFORE UPDATE ON ticket
    FOR EACH ROW EXECUTE PROCEDURE valid_ticket_status();

-- SET CURRENT DATE WHEN ADDING NEW ROW FOR TICKETS TABLE
CREATE or REPLACE FUNCTION add_cur_date() RETURNS trigger AS $add_cur_date$
    BEGIN
        NEW.create_date := now();
        NEW.update_date := now();
        RETURN NEW;
    END;
$add_cur_date$ LANGUAGE plpgsql;

CREATE TRIGGER add_cur_date BEFORE INSERT ON ticket
    FOR EACH ROW EXECUTE PROCEDURE add_cur_date();

CREATE or REPLACE FUNCTION add_date_creation_in_comment() RETURNS trigger AS $add_date_creation_in_comment$
    BEGIN
        NEW.date_creation := now();
        RETURN NEW;
    END;
$add_date_creation_in_comment$ LANGUAGE plpgsql;

CREATE TRIGGER add_date_creation_in_comment BEFORE INSERT ON comment
    FOR EACH ROW EXECUTE PROCEDURE add_date_creation_in_comment();

--- UPDATE comment SET text = 'new text' WHERE id = 6;
--- select * from comment;

CREATE or REPLACE FUNCTION check_ticket_status() RETURNS trigger AS $check_ticket_status$
    DECLARE
        ticket_status status_type;
    BEGIN
        SELECT ticket.status INTO ticket_status FROM ticket where ticket.id = NEW.ticket_id;
        IF ticket_status = 'CLOSED'::status_type THEN
            RAISE EXCEPTION 'This ticket was close';
        END IF;
        RETURN NEW;
    END;
$check_ticket_status$ LANGUAGE plpgsql;

CREATE TRIGGER check_ticket_status BEFORE INSERT or UPDATE ON comment
    FOR EACH ROW EXECUTE PROCEDURE check_ticket_status();

 INSERT INTO ticket (title, text, email) VALUES ('title1', 'text1', 'email1@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title2', 'text2', 'email2@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title3', 'text3', 'email3@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title4', 'text4', 'email4@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title5', 'text5', 'email5@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title6', 'text6', 'email6@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title7', 'text7', 'email7@test.ru');
 INSERT INTO ticket (title, text, email) VALUES ('title8', 'text8', 'email8@test.ru');

 INSERT INTO comment (email, text, ticket_id) VALUES ('email1@test.ru', 'text1', '1');
 INSERT INTO comment (email, text, ticket_id) VALUES ('email2@test.ru', 'text2', '1');
 INSERT INTO comment (email, text, ticket_id) VALUES ('email3@test.ru', 'text3', '3');
 INSERT INTO comment (email, text, ticket_id) VALUES ('email4@test.ru', 'text4', '2');
 INSERT INTO comment (email, text, ticket_id) VALUES ('email5@test.ru', 'text5', '2');
 INSERT INTO comment (email, text, ticket_id) VALUES ('email6@test.ru', 'text6', '3');
