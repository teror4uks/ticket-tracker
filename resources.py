import os
import json
from email.utils import parseaddr
from datetime import datetime
from flask_restful import reqparse, Resource, abort
import psycopg2
import psycopg2.extras
from flask import current_app as app

class Index(Resource):
    def get(self):
        return 200

class Ticket(Resource):
    def __init__(self, *args, **kwargs):
        super(Ticket, self).__init__(*args, **kwargs)
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('status')

    def put(self, ticket_id):
        result = None
        args = self.parser.parse_args()
        if any(k for k in args if args[k] is None):
            return {
                'result': 'Some args is None: {}'.format(args),
                'status': 400
            }, 400
        with app.db:
            with app.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
                SQL = "UPDATE ticket SET status=%s WHERE id=%s RETURNING id"
                SQL_SELECT_TICKET = "SELECT * FROM ticket WHERE id=%s"
                SQL_SELECT_COMMENTS = "SELECT * FROM comment WHERE ticket_id=%s"
                data = (args['status'], ticket_id)
                try:
                    curs.execute(SQL, data)
                    _id = curs.fetchone()
                    
                    if _id is None:
                        return {
                            'result': 'Ticket with id: %s not exist' % _id,
                            'status': 400
                        }, 400
                    # for cache
                    curs.execute(SQL_SELECT_TICKET, (ticket_id,))
                    ticket = curs.fetchone()
                    ticket = dict(ticket)
                    curs.execute(SQL_SELECT_COMMENTS, (ticket_id,))
                    ticket['comments'] = [dict(i) for i in curs]
                    ticket = json.loads(json.dumps(ticket, indent=4, sort_keys=True, default=str))
                    app.cache.set(str(ticket_id), ticket, timeout=5*60)
                    result = dict(result=ticket, status=200) 
                    return result, 200
                except psycopg2.DataError as why:
                    error_message = why.message.split('\n')[0]
                    return {
                        'result': error_message,
                        'status': 400
                    }, 400
                except psycopg2.InternalError as why:
                    error_message = why.message.split('\n')[0]
                    return {
                        'result': error_message,
                        'status': 400
                    }, 400

        return result, 400

    def get(self, ticket_id):      
        result = []
        ticket = app.cache.get(str(ticket_id))
        if ticket:
            return {
                'result': ticket,
                'status': 200
            }, 200

        with app.db:
            with app.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
                # select * from ticket right join comment on ticket.id = comment.ticket_id WHERE ticket.id = %s;
                SQL = "SELECT * FROM ticket WHERE id = %s"
                SQL_COMMENTS = "SELECT * FROM comment WHERE ticket_id = %s"
                data = (ticket_id,)
                curs.execute(SQL, data)    
                row = curs.fetchone()
                if row is None:
                    return {
                        'result': 'Ticket not exist', 
                        'status': 400
                    }, 400
                curs.execute(SQL_COMMENTS, (ticket_id,))
                row = dict(row)
                row['comments'] = []
                for comment in curs:
                    row['comments'].append(dict(comment))
                row = json.loads(json.dumps(row, indent=4, sort_keys=True, default=str))
                app.cache.set(str(ticket_id), row, timeout=5*60)
                result = dict(result=row, status=200)
                return result, 200
        return result, 400

    
class TicketList(Resource):
    def __init__(self, *args, **kwargs):
        super(TicketList, self).__init__(*args, **kwargs)
        self.parser = reqparse.RequestParser()
        map(lambda x: self.parser.add_argument(x), ('title', 'text', 'email'))

    def get(self):
        result = []
        with app.db:
            with app.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
                SQL = "SELECT * FROM ticket;"
                curs.execute(SQL)
                for row in curs:
                    result.append(dict(row))
                result = dict(result=result,status=200)
                result = json.loads(json.dumps(result, indent=4, sort_keys=True, default=str))
                return result, 200
        return result, 400

    def post(self):
        result = None
        args = self.parser.parse_args()
        if any(k for k in args if args[k] is None):
            return {
                'result': 'Some args is None: {}'.format(args),
                'status': 400
            }, 400

        with app.db:
            with app.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
                SQL = "INSERT INTO ticket (title, text, email) VALUES (%s, %s, %s) RETURNING id"
                SQL_SELECT = "SELECT * FROM ticket WHERE id=%s"
                data = (args['title'], args['text'], args['email'])
                curs.execute(SQL, data)
                ticket_id = curs.fetchone()[0]
                curs.execute(SQL_SELECT, (ticket_id,))
                ticket = curs.fetchone()
                ticket = json.loads(json.dumps(dict(ticket), indent=4, sort_keys=True, default=str))
                result = dict(result=ticket, status=201)
                return result, 201
        return result, 400


class Comment(Resource):
    def __init__(self, *args, **kwargs):
        super(Comment, self).__init__(*args, **kwargs)
        self.parser = reqparse.RequestParser()
        map(lambda x: self.parser.add_argument(x), ('text', 'email'))

    def post(self, ticket_id):
        result = None
        args = self.parser.parse_args()
        if any(k for k in args if args[k] is None):
            return {
                'result': 'Some args is None: {}'.format(args),
                'status': 400
            }, 400
        with app.db:
            with app.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
                SQL = "INSERT INTO comment (text, email, ticket_id) VALUES (%s, %s, %s) RETURNING id"
                SQL_SELECT = "SELECT * FROM comment WHERE id=%s;"
                data = (args['text'], args['email'], ticket_id)
                try:
                    curs.execute(SQL, data)
                    comment_id = curs.fetchone()[0]
                    curs.execute(SQL_SELECT, (comment_id,))
                    comment = curs.fetchone()
                    comment = json.loads(json.dumps(dict(comment), indent=4, sort_keys=True, default=str))
                except psycopg2.InternalError as why:
                    error_message = why.message.split('\n')[0]
                    return {
                        'result': error_message,
                        'status': 400
                    }, 400
                result = dict(result=comment, status=201)
                return result, 201
        
        return result, 400